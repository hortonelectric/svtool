import Vue from 'vue'
import Vuetify from 'vuetify'
import Snotify from 'vue-snotify'
import 'vuetify/dist/vuetify.min.css'
import 'vue-snotify/styles/material.css'

import App from './App.vue'
import router from './router'
import store from './store'

Vue.use(Vuetify)
Vue.use(Snotify, {
  toast: {
    position: 'rightTop'
  }
})
// {
//   theme: {
//     primary: '#8BC349',
//     secondary: '#775BA7',
//     accent: '#ffd54e',
//     error: '#E82064',
//     info: '#38A4DD',
//     success: '#38A4DD',
//     warning: '#f89724'
//   }
// }
Vue.config.productionTip = false

new Vue({ // eslint-disable-line no-new
  el: '#app',
  store,
  router,
  render: h => h(App)
})
