import Vue from 'vue'
import Vuex from 'vuex'

import ui from './ui'
import data from './data'
const modules = { ...ui, ...data }

Vue.use(Vuex)

export default new Vuex.Store({
  modules,
  strict: process.env.NODE_ENV !== 'production'
})
