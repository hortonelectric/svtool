import { connect } from '../../api/socket.js'

const state = {
}

const getters = {
}

const mutations = {
}

const actions = {
  startApp: ({ dispatch }) => {

  },
  connectWebSocket: (context) => {
    try {
      connect(context)
    } catch (error) {
      console.error('SOCKET ERROR', error)
    }
  },
  getDumps: async (context) => {
    // const { auth } = context.rootState.User
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
