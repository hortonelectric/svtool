import _ from 'lodash'
import * as api from '../../api/index.js'

const state = {
  analyticsData: [],
  searchAnalytics: '',
  remoteParams: {}
}
const getters = {
  getAnalyticsData: (state, getters, rootState) => {
    return _.map(state.analyticsData, key => {
      return {
        ...key
      }
    })
  }
}
const mutations = {
  setAnalyticsData: (state, value) => {
    for (var i = 0; i < value.length; i++) {
      // console.log(value[i].data)
      for (var x = 0; x < value[i].data.length; x++) {
        let thisData = value[i].data[x]
        let parsedData = []
        for (var y = 0; y < Object.keys(thisData).length; y++) {
          parsedData.push({data: thisData[y], isJSON: typeof (thisData[y]) !== 'string'})
        }

        let previewString
        if (parsedData[0].data.isJSON) {
          previewString = 'JSON Data---'
        } else {
          if (parsedData[0].data.length > 25) {
            previewString = parsedData[0].data.substring(0, 25) + '...'
          } else {
            previewString = parsedData[0].data
          }
        }

        value[i].data[x] = {
          date: value[i].createdAt[x],
          data: parsedData,
          previewString,
          id: value[i].id[x]
        }
      }
      // console.log(value[i].data)
    }
    state.analyticsData = value
  },
  setSearchAnalytics: (state, value) => {
    state.searchAnalytics = value
  },
  setRemoteParams: (state, value) => {
    state.remoteParams = value
  }
}

const actions = {
  refreshAnalytics: async ({state, rootState, commit, dispatch}) => {
    commit('startGlobalFetch')
    try {
      let results = await api.analyticsAdmin(state.remoteParams)
      commit('setAnalyticsData', results)
      commit('stopGlobalFetch')
    } catch (e) {
      console.error('Analytics fetch error', e)
      commit('stopGlobalFetch')
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
