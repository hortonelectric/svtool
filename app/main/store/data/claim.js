import _ from 'lodash'
import * as api from '../../api/index.js'

const Bitcore = require('bitcore-lib-cash')
const Address = Bitcore.Address
const HDPrivateKey = Bitcore.HDPrivateKey
const PrivateKey = Bitcore.PrivateKey
const Unit = Bitcore.Unit
const Networks = Bitcore.Networks
const Transaction = Bitcore.Transaction
const Mnemonic = require('bitcore-mnemonic-cash')
let deviceObj
let deviceId = 'node-webkit'
let devicePlatform = 'node-webkit'
let deviceModel = 'node-webkit'
try {
  if (device !== undefined) {// eslint-disable-line
    deviceObj = device// eslint-disable-line
    if (device.platform) {// eslint-disable-line
      deviceId = device.platform// eslint-disable-line
      devicePlatform = device.platform// eslint-disable-line
    }
    if (device.model) {// eslint-disable-line
      deviceModel = device.model// eslint-disable-line
    }
    if(device.uuid) {// eslint-disable-line
      deviceId = device.uuid// eslint-disable-line
    }
  }
} catch (e) {
  console.error('no device ID')
}
if (!window.QRScanner && typeof device === 'undefined') {
  const QRScanner = require('../../lib/qrscanner.min.js')
  window.QRScanner = QRScanner.QRScanner
}
const svWIFArray = [
  {'privkey': 'Kys6hmJdh37AfS6nNG7VFwh1DXLjEnPf6kSt5bkxFpbYN9qJKams', 'address': '16omjCBe21CNApCi4oxFToWmnbNaXsyhnV', 'path': '0/7', 'cashaddr': 'bitcoincash:qqlmzxv2fjz6djfzsqzjuytws2y4gggtxu7fr4uar9'},
  {'privkey': 'KwrUCkk1yB6ozYkT8wnBFUrcri7CHg9wtFEzEu1xc3x1YeyVCRWS', 'address': '1CFdnE8bDXgoYMnNb3NEzFGt7wmbTKVnVu', 'path': '0/8', 'cashaddr': 'bitcoincash:qpakm6x8ywdgurdryktfmdk33rfjke0l7v6jaz5uaf'},
  {'privkey': 'KyKkTWjSP9ZoYLYXfjQ2vrMbnoCobPPfVZYCTxQqY1BisUt3YNcZ', 'address': '13DBNTvRn3hd7FaeWhJF9DFJEu6K12ib9d', 'path': '0/9', 'cashaddr': 'bitcoincash:qqvrmejvc43ugk324nckjsrlshc0zur9c5c6p5tgvv'},
  {'privkey': 'KzXqh5o7LCEbNVjr1NPMoDMqQnLthi5w9PYReuT6gi1WhR5Y4buK', 'address': '1Nr1GXmzyQ6oQQTsT21d2Zd94ccK2F5reP', 'path': '0/10', 'cashaddr': 'bitcoincash:qrhe706zjgnuy44zash7qdepr9gzcpg7euajlwvnge'},
  {'privkey': 'KxjTeKiHfRd8j5onfLwn9ymjmJeC3iyQn7bvvCt8g3Zo8NmTnyBc', 'address': '18DuukgX8mJas4g5Zo2E7SU8xPeZCehV8K', 'path': '0/11', 'cashaddr': 'bitcoincash:qp8n5z08gy3hpmhj8hgjzpvgdld5pee4gyd36kh5jx'},
  {'privkey': 'Kyw2M2J7YcPcdZiqQyWFAKuikRh1XsFeHa8BDn66hYWL81igkscD', 'address': '14BMcYDXLAZwHrV8bEHqBiDk9A7Q2C512h', 'path': '0/12', 'cashaddr': 'bitcoincash:qq3dm0jazs654tj26f0r97n4get0aujez5hav8v2k5'},
  {'privkey': 'Ky2kK6HgzMLjcpiDZeR27RgG6XxSZWCm78RAKYNUU8FQv13owMB2', 'address': '1QCQm2SZdqLUDEmTjmVYzuAoZVpdTkfey1', 'path': '0/13', 'cashaddr': 'bitcoincash:qrl8xnfekcmjtspn4lpueue37v5laz78f5358cqjcs'},
  {'privkey': 'KwzvUphN1G4u2F8R5suQNepxbGBdoZZ7Z2uRzzBzv89TQVkdcvfW', 'address': '1AjU2frczQEiNeFYuPLeZ57TBwWqT8u4Fv', 'path': '0/14', 'cashaddr': 'bitcoincash:qp4vz236pncqlpd9cjh7u5s8zg8w5gu2dupwxrhzvf'},
  {'privkey': 'L4d3nizvYa3PaRSzJSeKGmjxWzxUf1biwVbfQqp22wapV352HCSd', 'address': '1SbmS4o3stE5t6wSw2hjuTug6kpWjuRF9', 'path': '0/15', 'cashaddr': 'bitcoincash:qqzdw6uccmsxkxr5gnfmwk7vahtmy8sry560gzf988'},
  {'privkey': 'L5HsnC1XuEFeaBa8DyFgRwJqbm8GGChwHMZhRk1iutA4WGR8izMT', 'address': '1NApzSEQ8WgGkwsXTAKMS67AioBGD4PLvr', 'path': '0/16', 'cashaddr': 'bitcoincash:qr5rvusrs39qg3agrzqu3fmukya5d94rcyc6vsq2d9'},
  {'privkey': 'KyqjghH9eYbKkKhAnhtJZ8JmcgZxfPmEGfgatc3489bdGccJdLtE', 'address': '1Ng9jPwMNMhqBwsJKdGb1CkyE73tgJUSuH', 'path': '0/17', 'cashaddr': 'bitcoincash:qrkuyfhkme6jj3f69y2xm90jzd34wv7cq5hseph2xu'},
  {'privkey': 'L1t7YZcqpy3APjXgukb49sSoaKu3cy8XnvkryzhgKVN6ybu8AQ5c', 'address': '1FsMfAk81Uee8vTcPdfgw9G2n9BajNbpKP', 'path': '0/18', 'cashaddr': 'bitcoincash:qz330nd4dlc92xtv0u5hlqmtlaxk37te4sgy8ns60w'},
  {'privkey': 'KyYzskY4hc83roC6HiJnYKQAo2B21A81jYVqFNLPVM6bKphD28LK', 'address': '1EBeZXsGPEZEDFoizFL7bDyb9KhjrYEzdh', 'path': '0/19', 'cashaddr': 'bitcoincash:qzgf6f3fgtta6hxy65rll4p70jf3c3c7v504cwf2r3'}
]
const svFundingAddress = 'bitcoincash:qqlmzxv2fjz6djfzsqzjuytws2y4gggtxu7fr4uar9'
const abcWIFArray = [
  {'path': '1/7', 'address': '1AEvLi67nXirjhXKMmLDz4vEAR6xZY7N9k', 'privkey': 'L4xb6bYVYZ5so3o5CUHJ8DwV96vmYYSVaxZRoNBSeW1SRpZpdqXb', 'cashaddr': 'bitcoincash:qpj4ky9uc2xvu7e80c0rdm8z6jcnt5846vvkezvv44'},
  {'path': '1/8', 'address': '14k1sY4aKq8yXeUjjptJgY4wRYSdUtEtFg', 'privkey': 'L3Y2UZi8rYbQms5GimfMZ2K5WXideZh1TKi91NGB128GgPS3SXGS', 'cashaddr': 'bitcoincash:qq5s4ld5wlv4zdwjk93hclj0n9e847k0fy8fq604mm'},
  {'path': '1/9', 'address': '1E7u3Dg9oR1TAPgFbnNsXavbvjxjtyNNw6', 'privkey': 'KzQxXtq1acnvq2drfrCw1keWv6fFqcSB3JyAhDPXuXJPL7FHfpTd', 'cashaddr': 'bitcoincash:qz870y0lduzxsjh7arpu6j7mtu2vx937tqzg9r0j9h'},
  {'path': '1/10', 'address': '1428jvVYwZZoT1TeNkvMGKJSXJMckcdikQ', 'privkey': 'KyaaeDUdykQA8KUwPqQxhf1S6SPJLvDwBRsURxy1tRJpa6ZzPq3f', 'cashaddr': 'bitcoincash:qqs37sg73auqzv83wru70j46g8jldna4ccr637nrrt'},
  {'path': '1/11', 'address': '1Q8GrP334kUNRAEtSKo3svi4NZgaurFzLc', 'privkey': 'L4634qx3VrGv2soAUF42rvzU1DDy1MAnRqZcb6r6bAzz95RigT6T', 'cashaddr': 'bitcoincash:qr76kztdgqwjmt5jmcv9yaax643pz3dlv54cwswwau'},
  {'path': '1/12', 'address': '17B83z3p7UiZ7oHBqPkH5eTqZzvQyjpFej', 'privkey': 'KzPiUz2bc67AMF3JnpMwcJ3un8MtKMZjNoA1YLwo83vm9fKMy9CL', 'cashaddr': 'bitcoincash:qppm4n83ltzp223mp4cqjaay2q229650lunh2vpqax'},
  {'path': '1/13', 'address': '1KGjUxCfS1yfkNnXDGqKTnoWSsEF8fSgPR', 'privkey': 'KwpmFGnbLJ7wyCNswcwFjtpHouTipUNoYKTMi6st4hNva2PfSm6u', 'cashaddr': 'bitcoincash:qryxhae4jsatqpjv430sc4k5p3knxht56gmwtkkxu3'},
  {'path': '1/14', 'address': '17Pyok2QMnEMWajJky3gfLdnFrQFT5Cd4p', 'privkey': 'L5FNJ4kN5osnf2nw94psYwTQouZ5TTpMbBQqc8Q84h5d5wryMMJD', 'cashaddr': 'bitcoincash:qprzj5e4vkkelrnm9kkxa6l8dcmt3pfuuyud0utg8f'},
  {'path': '1/15', 'address': '1BXGxwEoaDn6EZQBFMFpuM3M8zgrmBzBPy', 'privkey': 'KyZNYjRxtSkmicnzx6afw6t44FrpJ4wCcGAm9VHzXRzXbqYnXJYy', 'cashaddr': 'bitcoincash:qpekkyp8t0r6gtnyfkkfdujn2u6wj2xg3v9xjkpy2h'},
  {'path': '1/16', 'address': '19f7CFjBKT4hwvTMvEyKJn4Z8cqZ1e2dTb', 'privkey': 'L1Xxgm1c7oCVqJWEeRYbfMnbh8GTWd7AsFpDJwYYVzSzpfcfNJbr', 'cashaddr': 'bitcoincash:qp00t7am7tcja8x7dwufry6vyl82nxgpmgw88es3wm'},
  {'path': '1/17', 'address': '16kqSpatNjpxWs9WSFscysGAKxDSWXb3Ff', 'privkey': 'KxxgUKiD9nvCMEcYNUKb7d3cST593s6Yk1jGfH726asx73ZBMNwZ', 'cashaddr': 'bitcoincash:qqlj9ayvatvy7j6g7tmcfmuxjlmp2gr7qu7sqmedvd'},
  {'path': '1/18', 'address': '1iu5geVFqWjPDXNP9nKgzV7pSzSbGS1jB', 'privkey': 'L5WQm7Pvd3RQC3ntvCky7NpFwodMePZjgLuVXftmHhp3jr7HibAS', 'cashaddr': 'bitcoincash:qqr7epma8lcj0f54rdgkmtmtf6pczd86gqrs4ssccz'},
  {'path': '1/19', 'address': '124xksqJ17RWHH5kytDvcrGxTZux6c9Ken', 'privkey': 'L144mTDhvctstdc7iLYYFGc2Uusvw6DsVQCXFbQnJMyLoJq2mqrQ', 'cashaddr': 'bitcoincash:qq9m064k803np00yngkum2dvhm8nqwnxcy5duvmurf'}
]
const abcFundingAddress = 'bitcoincash:qpj4ky9uc2xvu7e80c0rdm8z6jcnt5846vvkezvv44'

const state = {
  svAddress: '',
  abcAddress: '',
  email: '',
  txParts: 0,
  mnemonic: '',
  xprivkey: '',
  privateKeysInputLimitArr: [],
  notificationMaxInputs: '',
  xprivkeyMaxInputs: 250,
  mnemonicMaxInputs: 250,
  total: '0 BCH SV/ABC',
  privateKeys: [],
  longestChain: '',
  longestChainHeight: '',
  notificationSweepKeys: '',
  notificationBuildTx: '',
  notificationChainLength: '',
  sentNotificationSV: '',
  sentNotificationABC: '',
  rawtxSV: '',
  rawtxABC: '',
  ratesSV: {},
  ratesABC: {},
  svInfo: {},
  abcInfo: {},
  derivationPaths: ["m/44'/0'/0'", "m/44'/145'/0''", "m/44'/60'/0'"],
  derivationPath: "m/44'/0'/0'",
  derivationPathXprivkey: 'm',
  addressRange: '0-125',
  addressRangeXprivkey: '0-125',
  cameraMode: '',
  isCameraAvailable: false
}
if (typeof device === 'undefined') {// eslint-disable-line
  state.isCordova = false
  state.isCameraAvailable = true // node-webkit
} else if (device.cordova) {// eslint-disable-line
  state.isCordova = true
  if (device.platform === 'osx') {// eslint-disable-line
    state.isCameraAvailable = false
  } else {
    state.isCameraAvailable = true
  }
}

state.debug = false

if (state.debug) {
  // state.email = 'hortonelectric@gmail.com'
  // state.svAddress = 'qzv3x89kpq2wqch0qugpvtqxa5hwpz68lqxpggus2m'
  // state.abcAddress = 'qqlmzxv2fjz6djfzsqzjuytws2y4gggtxu7fr4uar9'
  // state.mnemonic = 'dial nice talent arrive ceiling raw lemon unaware exact flip robust cage'
  // state.xprivkey = 'xprv9wU79XcEPUZYXQ9p7fUo6S2VhpjcK7dazkLRz4iuaErEceDSYUcssRM7nQpEGGjqKbGmoqyKhGj6xrJZ9NRnYqdr5ZCpA7WQXz973bu2ek9'
  state.xprivkeyMaxInputs = 5
  state.mnemonicMaxInputs = 5
  // state.privateKeys = ['L3smPTeAbPjSS5jUwpJC1Y5WEMXNFiywGLZM7zCCujMGsTxhXpk5', 'L3PscmrLqK2VW6BuZUUduP6aqhf4ygEEMGw494p7xpp1wY6gKVqf', 'KyHH2ASKNErCs3dRTjL4eZs3pkTQ6xnkxhTQimBpxVWsxCBVemhW']
  // state.privateKeysInputLimitArr = [1, 1, 1]
}
const microAmount = 0.00005460
const microAmountSatoshi = 5460
let longestChain

let longestChainHeight = 500
let shortestChainHeight = 10

let total = 0
let totalToSend = 0
let parsedUtxos = []
let privKeysToSpend = []

let logObj = []
const getters = {

}

const mutations = {
  setDebug: (state, value) => {
    state.debug = !state.debug
    if (state.debug) {
      // state.email = 'hortonelectric@gmail.com'
      // state.svAddress = 'qzv3x89kpq2wqch0qugpvtqxa5hwpz68lqxpggus2m'
      // state.abcAddress = 'qqlmzxv2fjz6djfzsqzjuytws2y4gggtxu7fr4uar9'
      // state.mnemonic = 'dial nice talent arrive ceiling raw lemon unaware exact flip robust cage'
      // state.xprivkey = 'xprv9wU79XcEPUZYXQ9p7fUo6S2VhpjcK7dazkLRz4iuaErEceDSYUcssRM7nQpEGGjqKbGmoqyKhGj6xrJZ9NRnYqdr5ZCpA7WQXz973bu2ek9'
      state.xprivkeyMaxInputs = 5
      state.mnemonicMaxInputs = 5
      // state.privateKeys = ['L3smPTeAbPjSS5jUwpJC1Y5WEMXNFiywGLZM7zCCujMGsTxhXpk5', 'L3PscmrLqK2VW6BuZUUduP6aqhf4ygEEMGw494p7xpp1wY6gKVqf', 'KyHH2ASKNErCs3dRTjL4eZs3pkTQ6xnkxhTQimBpxVWsxCBVemhW']
      // state.privateKeysInputLimitArr = [1, 1, 1]
    } else {
      // state.email = ''
      // state.svAddress = ''
      // state.abcAddress = ''
      // state.mnemonic = ''
      // state.xprivkey = ''
      state.xprivkeyMaxInputs = 250
      state.mnemonicMaxInputs = 250
      // state.privateKeys = []
      // state.privateKeysInputLimitArr = []
    }
  },
  resetSweep: (state, value) => {
    state.txParts = 0
    state.notificationMaxInputs = ''
    state.notificationChainLength = ''
    state.notificationBuildTx = ''
    state.sentNotificationABC = ''
    state.sentNotificationSV = ''
    state.notificationSweepKeys = ''
    state.rawtxSV = ''
    state.rawtxABC = ''
    parsedUtxos = []
    privKeysToSpend = []
  },
  setEmail: (state, value) => {
    state.email = value
  },
  setTxParts: (state, value) => {
    state.txParts = value
  },
  setMnemonicMaxInputs: (state, value) => {
    state.mnemonicMaxInputs = value
  },
  setXprivkeyMaxInputs: (state, value) => {
    state.xprivkeyMaxInputs = value
  },
  setCameraMode: (state, value) => {
    state.cameraMode = value
  },
  setNotificationChainLength: (state, value) => {
    state.notificationChainLength = value
  },
  setLongestChainHeight: (state, value) => {
    state.longestChainHeight = value
  },
  setXprivkey: (state, value) => {
    state.xprivkey = value
  },
  setSVInfo: (state, value) => {
    state.svInfo = value
  },
  setABCInfo: (state, value) => {
    state.abcInfo = value
  },
  setSentNotificationABC: (state, value) => {
    state.sentNotificationABC = value
  },
  setSentNotificationSV: (state, value) => {
    state.sentNotificationSV = value
  },
  setLongestChain: (state, value) => {
    state.longestChain = value
  },
  setDerivationPath: (state, value) => {
    state.derivationPath = value
  },
  setAddressRange: (state, value) => {
    state.addressRange = value
  },
  setDerivationPathXprivkey: (state, value) => {
    state.derivationPathXprivkey = value
  },
  setAddressRangeXprivkey: (state, value) => {
    state.addressRangeXprivkey = value
  },
  setRatesSV: (state, value) => {
    state.ratesSV = value
  },
  setRatesABC: (state, value) => {
    state.ratesABC = value
  },
  setRawtxSV: (state, value) => {
    state.rawtxSV = value
  },
  setRawtxABC: (state, value) => {
    state.rawtxABC = value
  },
  setDecodedTx: (state, value) => {
    state.decodedTx = value
  },
  setNotificationBuildTx: (state, value) => {
    state.notificationBuildTx = value
  },
  setNotificationSweepKeys: (state, value) => {
    state.notificationSweepKeys = value
  },
  setSvAddress: (state, value) => {
    state.svAddress = value
  },
  setAbcAddress: (state, value) => {
    state.abcAddress = value
  },
  setMnemonic: (state, value) => {
    state.mnemonic = value
  },
  addPrivateKey: (state, value) => {
    state.privateKeys.push(value)
    if (state.debug) {
      state.privateKeysInputLimitArr.push(1)
    } else {
      state.privateKeysInputLimitArr.push(250)
    }
  },
  setPrivateKeyInputLimit: (state, value) => {
    state.privateKeysInputLimitArr[value.index] = value.limit
  },
  setPrivateKeyInputLimitArr: (state, value) => {
    state.privateKeysInputLimitArr = value
  },
  setNotificationMaxInputs: (state, value) => {
    state.notificationMaxInputs = value
  },
  removePrivateKey: (state, index) => {
    state.privateKeysInputLimitArr.splice(index, 1)
    state.privateKeys.splice(index, 1)
  },
  clearForm: (state) => {
    state.privateKeys = []
    state.privateKeysInputLimitArr = []
    state.mnemonic = ''
    state.xprivkey = ''
    state.total = '0 SV/ABC'
    state.Rawtx = ''
    state.xprivkeyMaxInputs = 20
    state.mnemonicMaxInputs = 20
    state.decodedTx = {}
    state.svAddress = {}
    state.abcAddress = {}
    state.notification = {}
    state.sentNotificationSV = {}
    state.sentNotificationABC = {}
    state.notificationChainLength = {}
    state.derivationPath = "m/44'/0'/0'"
    state.addressRange = '0-125'
    state.derivationPathXprivkey = 'm'
    state.addressRangeXprivkey = '0-125'
    state.cameraMode = ''
    state.txParts = 0
    state.notificationMaxInputs = ''
  }
}

async function log () {
  console.info(...arguments)
  let logObject = {}
  if (deviceObj) {
    logObject = {
      device: deviceObj,
      model: deviceModel,
      platform: devicePlatform,
      appId: deviceId
    }
  }
  logObject.data = JSON.stringify(arguments)
  await api.analytics(logObject)
  for (var arg = 0; arg < arguments.length; arg++) {
    if (typeof arguments[arg] === 'string') {
      logObj.push(arguments[arg])
    } else {
      logObj.push(JSON.stringify(arguments[arg]))
    }
  }
}

async function logError () {
  console.error(...arguments)
  let logObject = {}
  if (deviceObj) {
    logObject = {
      device: deviceObj,
      model: deviceModel,
      platform: devicePlatform,
      appId: deviceId
    }
  }
  logObject.data = JSON.stringify(arguments)
  await api.analytics(logObject)
  for (var arg = 0; arg < arguments.length; arg++) {
    if (typeof arguments[arg] === 'string') {
      logObj.push(arguments[arg])
    } else {
      logObj.push(JSON.stringify(arguments[arg]))
    }
  }
}
const actions = {
  refreshRates: async ({state, rootState, commit, dispatch}) => {
    try {
      let resultSV = await api.ratesSV()
      commit('setRatesSV', resultSV)
      let resultABC = await api.ratesABC()
      commit('setRatesABC', resultABC)
      log('App Opened')
    } catch (e) {
      logError('Rate fetch error')
    }
  },
  sweepKeysError: async ({state, rootState, commit, dispatch}, message) => {
    commit('setNotificationSweepKeys', message)
    logError(message)
    commit('stopGlobalFetch')
  },
  buildTxError: async ({state, rootState, commit, dispatch}, message) => {
    commit('setNotificationBuildTx', message)
    logError(message)
    commit('stopGlobalFetch')
  },
  sweepKeys: async ({state, rootState, commit, dispatch}) => {
    const { xprivkey, privateKeys, mnemonic, derivationPath, addressRange, derivationPathXprivkey, addressRangeXprivkey, privateKeysInputLimitArr, xprivkeyMaxInputs, mnemonicMaxInputs } = state

    commit('setStatus', '')
    commit('setTxParts', 0)
    commit('setRawtxSV', '')
    commit('setRawtxABC', '')
    commit('setNotificationChainLength', '')
    commit('setNotificationMaxInputs', '')
    commit('setNotificationSweepKeys', '')

    logObj = []
    log('Starting sweep: ' + Date().toString())
    total = 0
    totalToSend = 0
    parsedUtxos = []
    privKeysToSpend = []

    if (!xprivkey && !mnemonic && privateKeys.length < 1) {
      return dispatch('sweepKeysError', 'Select at least one import method and enter key information first.')
    }
    commit('startGlobalFetch')

    commit('setStatus', 'Checking nodes status')

    let privKeysUnspentMN = []
    let privKeysUnspentXPRV = []
    let privKeysUnspentPK = []

    let svInfo, abcInfo

    try {
      svInfo = await api.infoSV()
      abcInfo = await api.infoABC()
      log('Node Status', svInfo, abcInfo)
      if (parseInt(svInfo.timeoffset, 10) !== 0 || parseInt(abcInfo.timeoffset, 10) !== 0) {
        return dispatch('sweepKeysError', 'timeoffset on one of our bitcoind nodes is > 0. Please contact the system admin.')
      }
      commit('setSVInfo', svInfo)
      commit('setABCInfo', abcInfo)

      let svHeight = svInfo.blocks
      let abcHeight = abcInfo.blocks
      if (svHeight > abcHeight) {
        longestChainHeight = svHeight
        longestChain = 'sv'

        shortestChainHeight = abcHeight
      } else {
        longestChainHeight = abcHeight
        shortestChainHeight = svHeight
        longestChain = 'abc'
      }

      if (longestChainHeight - shortestChainHeight < 6) {
        commit('setNotificationChainLength', 'WARNING! SV/ABC block height is close (only ' + (longestChainHeight - shortestChainHeight) + ' blocks apart). You might not have sufficient replay protection! Wait, or risk it (3 blocks is usually enough)')
      }

      if (!longestChainHeight) {
        return dispatch('sweepKeysError', 'Could not get info for lock time. Aborting.')
      }

      commit('setLongestChain', longestChain)
      log('long chain:' + longestChain + ':' + longestChainHeight)
    } catch (e) {
      return dispatch('sweepKeysError', 'Server connection error. Please try again or contact the system administrator if this problem persists')
    }

    const confNumber = longestChainHeight - 556765
    function parseUtxos (utxos, addrs, privKeysUnspent) {
      for (i = 0; i < utxos.length; i++) {
        let thisutxo = utxos[i]
        if (thisutxo.confirmations && parseInt(thisutxo.confirmations, 10) > confNumber && parseInt(thisutxo.height, 10) < 556765) {
          const keyIndex = _.findIndex(addrs, function (o) { return o === thisutxo.address })
          if (keyIndex < 0) {
            return dispatch('sweepKeysError', 'Unable to sign some inputs. tried to match ' + thisutxo.address + ' in addrs array, got -1')
          }
          privKeysToSpend.push(privKeysUnspent[keyIndex])

          total += thisutxo.satoshis
          parsedUtxos.push(thisutxo)
        }
      }
    }
    let addrsMnemonic = []
    let addrsXprivkey = []
    let addrsPrivatekeys = []

    if (mnemonic) {
      commit('setStatus', 'Deriving addresses from mnemonic')
      await log('Deriving addresses from mnemonic')
      let mnemonicValid = Mnemonic.isValid(mnemonic)

      if (!mnemonicValid) {
        return dispatch('sweepKeysError', 'Mnemonic is invalid')
      }

      const code = new Mnemonic(mnemonic)
      const seed = code.toSeed()
      let HDKey
      try {
        /* eslint new-cap: ["error", { "newIsCap": false }] */
        HDKey = HDPrivateKey.fromSeed(seed, Networks.get('livenet'))
      } catch (e) {
        logError('MNEMONIC HD KEY ERROR', e)
        return dispatch('sweepKeysError', 'Error generating Bip32 private key from mnemonic seed')
      }

      let rangeStart = 0
      let rangeEnd = 0
      try {
        const addressRangeArray = addressRange.split('-')
        if (addressRangeArray.length < 2) {
          return dispatch('sweepKeysError', 'Invalid address range. Example 0-125')
        }
        rangeStart = addressRangeArray[0]
        rangeEnd = addressRangeArray[1]
        if (rangeEnd - rangeStart > 125) {
          return dispatch('sweepKeysError', 'Invalid address range. Maximum range = 125')
        }
      } catch (e) {
        logError('MNEMONIC INVALID ADDRESS RANGE ERROR', e)
        return dispatch('sweepKeysError', 'Invalid address range. Example 0-125')
      }
      for (var i = rangeStart; i < rangeEnd; i++) {
        let keyMain, keyChange
        try {
          keyMain = HDKey.derive(derivationPath + '/0/' + i)
          keyChange = HDKey.derive(derivationPath + '/1/' + i)
        } catch (e) {
          logError('MNEMONIC INVALID DERIVATION PATH ERROR', e)
          return dispatch('sweepKeysError', 'Invalid derivation path')
        }

        privKeysUnspentMN.push(keyMain.privateKey)
        privKeysUnspentMN.push(keyChange.privateKey)
        const network = Networks.get('livenet')
        const thisAddressMain = new Address(keyMain.publicKey, network).toString()
        const thisAddressChange = new Address(keyChange.publicKey, network).toString()

        addrsMnemonic.push(thisAddressMain.replace('bitcoincash:', ''))
        addrsMnemonic.push(thisAddressChange.replace('bitcoincash:', ''))
      }

      let utxosMnemonic = []
      if (addrsMnemonic.length > 0) {
        commit('setStatus', 'Checking balances from mnemonic...')
        try {
          const utxosMnemonicResult = await api.utxos({addrs: addrsMnemonic.join(',')})
          utxosMnemonic = _.take(utxosMnemonicResult, mnemonicMaxInputs)
          log(utxosMnemonic.length + ' Mnemonic UTXOs', utxosMnemonic)
          parseUtxos(utxosMnemonic, addrsMnemonic, privKeysUnspentMN)
        } catch (e) {
          logError('MNEMONIC UTXO FETCH ERROR', e)
          return dispatch('sweepKeysError', 'Unable to get Mnemonic UTXOs. If this took a long time, try reducing the address range.')
        }
      }
    }

    if (xprivkey) {
      commit('setStatus', 'Deriving addresses from xPrivKey')
      await log('Deriving addresses from xPrivKey')
      let HDKey
      try {
        /* eslint new-cap: ["error", { "newIsCap": false }] */
        HDKey = new HDPrivateKey(xprivkey, Networks.get('livenet'))
      } catch (e) {
        logError('XPRIVKEY HD KEY ERROR', e)
        return dispatch('sweepKeysError', 'Error generating wallet from xPrivKey')
      }
      let rangeStart = 0
      let rangeEnd = 0
      try {
        const addressRangeArray = addressRangeXprivkey.split('-')
        if (addressRangeArray.length < 2) {
          return dispatch('sweepKeysError', 'Invalid address range. Example 0-250')
        }
        rangeStart = addressRangeArray[0]
        rangeEnd = addressRangeArray[1]

        if (rangeEnd - rangeStart > 250) {
          return dispatch('sweepKeysError', 'Invalid Xprivkey address range. Maximum range = 250')
        }
      } catch (e) {
        logError('XPRIVKEY ADDRESS RANGE ERROR', e)
        return dispatch('sweepKeysError', 'Invalid Xprivkey address range. Example 0-250')
      }
      for (var i2 = rangeStart; i2 < rangeEnd; i2++) {
        let keyMain
        try {
          keyMain = HDKey.derive(derivationPathXprivkey + '/' + i2)
        } catch (e) {
          logError('XPRIVKEY DERIVATION PATH ERROR', e)
          return dispatch('sweepKeysError', 'Invalid Xprivkey derivation path')
        }

        privKeysUnspentXPRV.push(keyMain.privateKey)
        const network = Networks.get('livenet')
        const thisAddressMain = new Address(keyMain.publicKey, network).toString()
        addrsXprivkey.push(thisAddressMain.replace('bitcoincash:', ''))
      }

      let utxosXpriv = []

      if (addrsXprivkey.length > 0) {
        commit('setStatus', 'Checking balances from xPrivKey...')
        try {
          const utxosXprivResult = await api.utxos({addrs: addrsXprivkey.join(',')})
          utxosXpriv = _.take(utxosXprivResult, xprivkeyMaxInputs)
          log(utxosXpriv.length + ' xPrivKey UTXOs', utxosXpriv)
          parseUtxos(utxosXpriv, addrsXprivkey, privKeysUnspentXPRV)
        } catch (e) {
          logError('XPRIVKEY UTXO FETCH ERROR', e)
          return dispatch('sweepKeysError', 'Unable to get XPrivKey UTXOs. If this took a long time, try reducing the address range.')
        }
      }
    }

    if (privateKeys && privateKeys.length > 0) {
      commit('setStatus', 'Deriving addresses from private keys')
      await log('Deriving addresses from private keys')
      for (let k = 0; k < privateKeys.length; k++) {
        try {
          let thisPrivateKey = PrivateKey.fromWIF(privateKeys[k])
          privKeysUnspentPK.push(thisPrivateKey)
          addrsPrivatekeys.push(new Address(thisPrivateKey.publicKey).toString().replace('bitcoincash:', ''))
        } catch (e) {
          logError('PRIVATE KEY FROM WIF ERROR', e)
          try {
            let thisPrivateKey = new PrivateKey(privateKeys[k])
            privKeysUnspentPK.push(thisPrivateKey)
            addrsPrivatekeys.push(new Address(thisPrivateKey.publicKey).toString().replace('bitcoincash:', ''))
            log('non-WIF Key detected') //, privateKeys[k]
          } catch (e) {
            logError('NON-WIF PRIVATE KEY ERROR', e)
            return dispatch('sweepKeysError', 'Invalid private key: ' + privateKeys[k])
          }
        }
      }
      let utxosPrivatekeysArr = []
      if (addrsPrivatekeys.length > 0) {
        commit('setStatus', 'Checking balances from private keys...')
      }
      for (var pk = 0; pk < addrsPrivatekeys.length; pk++) {
        try {
          const utxosPrivatekeysResult = await api.utxos({addrs: addrsPrivatekeys[pk]})
          utxosPrivatekeysArr[pk] = _.take(utxosPrivatekeysResult, privateKeysInputLimitArr[pk])
          log('Private Key ' + pk + ' UTXOs returned LIMITED', utxosPrivatekeysArr[pk])
          parseUtxos(utxosPrivatekeysArr[pk], addrsPrivatekeys, privKeysUnspentPK)
        } catch (e) {
          logError('PRIVATE KEY UTXO FETCH ERROR ' + pk, e)
          return dispatch('sweepKeysError', 'Unable to get Private Key UTXOs. If this took a long time, try reducing the address range.')
        }
      }
    }

    log(addrsMnemonic.length + ' Mnemonic addresses. ' + addrsXprivkey.length + ' XPriv addresses' + addrsPrivatekeys.length + ' Private key addresses')

    // in case we want to convert to cashaddr before sending to block explorer???
    // let addrsCash = []
    // for(let a=0;a<addrsCash;a++) {
    //   let convertedUtxoAddress = await api.convertAddr({addr: thisutxo.address})
    //   thisutxo.address = convertedUtxoAddress.cashaddr
    // }
    if (parsedUtxos.length) {
      log('UTXOs after parse: ' + parsedUtxos.length, parsedUtxos)
    }

    // log(privKeysToSpend.length + ' private keys')
    privKeysToSpend = _.uniq(privKeysToSpend)
    log(privKeysToSpend.length + ' private keys (UNIQUE)')

    if (!parsedUtxos || parsedUtxos.length < 1) {
      return dispatch('sweepKeysError', 'The keys you entered contain 0 balance')
    }

    const txfee = parsedUtxos.length * 5 * 250 // 5 satoshis per byte, each output is 250 bytes

    let handlingFee = parseInt(total * 0.01, 10)
    totalToSend = parseInt(total - txfee - handlingFee, 10)
    log('totalToSend: ' + totalToSend + ' total:' + total + ' txfee:' + txfee + ' handlingFee:' + handlingFee)
    if (!totalToSend || totalToSend < 5460 || handlingFee < 5460) {
      return dispatch('sweepKeysError', 'Wallet does not have enough balance. Check form entries. Minimum is ~0.01')
    }
    commit('setStatus', 'Formatting addresses from available inputs...')
    let utxoCashAddrs = {}
    for (let a = 0; a < parsedUtxos.length; a++) {
      const orig = parsedUtxos[a].address

      if (utxoCashAddrs[orig]) {
        parsedUtxos[a].address = utxoCashAddrs[orig]
        continue
      }
      let convertedUtxoAddress = await api.convertAddr({addr: orig})
      parsedUtxos[a].address = utxoCashAddrs[orig] = convertedUtxoAddress.cashaddr
    }

    const totalDisplay = Unit.fromSatoshis(total).toBTC()
    const handlingFeeDisplay = Unit.fromSatoshis(handlingFee).toBTC()
    const txfeeDisplay = Unit.fromSatoshis(txfee).toBTC()

    var sweepKeysDisplay = totalDisplay + ' BCH/SV to sweep. ' + handlingFeeDisplay + ' handling fee. ' + txfeeDisplay + ' TX fee. Scroll down to broadcast or change form entries and re-sweep.'
    commit('setNotificationSweepKeys', sweepKeysDisplay)

    if (parsedUtxos.length > 250) {
      parsedUtxos = _.take(parsedUtxos, 250)
      commit('setNotificationMaxInputs', '-----WARNING----- Too many utxos available. Only 250 utxos can be swept at once. You can broadcast now, but you will need to wait for confirmations and then sweep again using the SAME key settings as you have now!')
    }
    commit('setTxParts', parsedUtxos.length)
    commit('stopGlobalFetch')
  },
  addPrivateKey: async ({state, rootState, commit, dispatch}) => {
    const { privateKey } = rootState.AddPrivateKey
    if (privateKey) {
      commit('addPrivateKey', privateKey)
    } else {
      throw Error('INVALID PRIVATE KEY')
    }
    commit('resetSweep')
  },
  sendTx: async ({state, rootState, commit, dispatch}) => {
    function buildTx (inputs, outputs, fee, privateKeys, changeAddress, longestChainHeight, network, longestChain) {
      var transaction

      if (network === longestChain) {
        transaction = new Transaction().lockUntilBlockHeight(longestChainHeight)
      } else {
        transaction = new Transaction()
      }
      commit('setStatus', 'Building transaction')
      transaction = transaction.from(inputs)
      // TODO: if possible add these OPCODES for triple-tier replay protection
      // let opcode = 'OP_CHECKDATASIG'
      // if(network === 'sv') {
      //   opcode = 'OP_MUL'
      // }
      for (let i = 0; i < outputs.length; i++) {
        var address = outputs[i].address
        var amount = outputs[i].amount

        transaction = transaction.to(address, amount)
      }

      transaction.change(changeAddress)

      transaction.fee(fee)
      for (var k = 0; k < privateKeys.length; k++) {
        commit('setStatus', 'Signing ' + network.toUpperCase() + ' Key ' + (k + 1) + ' of ' + privateKeys.length)
        transaction.sign(privateKeys[k])
      }
      return transaction
    }

    const { debug, svAddress, abcAddress, email } = state

    commit('startGlobalFetch')
    commit('setNotificationBuildTx', '')
    commit('setStatus', 'DO NOT CLOSE THE APP OR GO TO ANY OTHER APPS DURING THIS PROCESS!')

    const profitAddress = 'qpxat6vcrg55ryfcgclla59qy5angvaa8uar37ztqr' // from Frob wallet, send profits here

    if (!svAddress || !abcAddress) {
      return dispatch('buildTxError', 'Enter destination addresses first.')
    }

    commit('setStatus', 'Formatting destination addresses...')
    let svFormatted, abcFormatted
    try {
      svFormatted = await api.convertAddr({addr: svAddress})
      abcFormatted = await api.convertAddr({addr: abcAddress})
    } catch (e) {
      return dispatch('buildTxError', 'Connection error while formatting addresses. Check your entries and try again or contact the system administrator if this problem persists. ' + JSON.stringify(e))
    }
    if (svFormatted.cashaddr === abcFormatted.cashaddr) {
      return dispatch('buildTxError', 'Use DIFFERENT addresses for SV and ABC. NOT THE SAME ADDRESS')
    }

    let svMicroUtxos = []
    let svMicroKeys = []
    let svMicrocount = 0

    let abcMicroKeys = []
    let abcMicroUtxos = []
    let abcMicrocount = 0
    if (!debug) {
      commit('setStatus', 'DO NOT CLOSE THE APP OR GO TO ANY OTHER APPS DURING THIS PROCESS! Compiling pre-split micro-ouputs...')

      let allSVMicroAddrs = _.map(svWIFArray, function (x) {
        return x.cashaddr
      })

      let allABCMicroAddrs = _.map(abcWIFArray, function (x) {
        return x.cashaddr
      })

      try {
        const microSV = await api.microSV({addrs: allSVMicroAddrs})
        const microABC = await api.microABC({addrs: allABCMicroAddrs})

        for (var msv = 0; msv < microSV.length; msv++) {
          svMicrocount += microSV[msv].amount
          try {
            let convertedUtxoAddress = await api.convertAddr({addr: microSV[msv].address})
            microSV[msv].address = convertedUtxoAddress.cashaddr
          } catch (e) {
            return dispatch('buildTxError', 'SV microkey cashaddr translation network error')
          }

          svMicroUtxos.push(microSV[msv])
          let microThis = _.find(svWIFArray, {cashaddr: microSV[msv].address})
          if (!microThis) {
            return dispatch('buildTxError', 'unknown SV microkey generation error')
          }
          let thismicrokey = new PrivateKey(microThis.privkey)
          svMicroKeys.push(thismicrokey)
          if (svMicrocount >= microAmount) {
            break
          }
        }

        for (var mabc = 0; mabc < microABC.length; mabc++) {
          abcMicrocount += microABC[mabc].amount
          try {
            let convertedUtxoAddress = await api.convertAddr({addr: microABC[mabc].address})
            microABC[mabc].address = convertedUtxoAddress.cashaddr
          } catch (e) {
            return dispatch('buildTxError', 'ABC microkey cashaddr translation network error')
          }

          abcMicroUtxos.push(microABC[mabc])
          let microThis = _.find(abcWIFArray, {cashaddr: microABC[mabc].address})
          if (!microThis) {
            return dispatch('buildTxError', 'unknown SV microkey generation error')
          }
          let thismicrokey = new PrivateKey(microThis.privkey)
          abcMicroKeys.push(thismicrokey)
          if (abcMicrocount >= microAmount) {
            break
          }
        }

        if (!microSV || microSV.length < 1 || svMicroUtxos.length < 1 || svMicrocount < microAmount) {
          return dispatch('buildTxError', 'SV Micro-ouputs not available. Contact system administrator')
        }

        if (!microABC || microABC.length < 1 || abcMicroKeys.length < 1 || abcMicrocount < microAmount) {
          return dispatch('buildTxError', 'ABC Micro-ouputs not available. Contact system administrator')
        }
        // log(microSV.length + ' SV micro outputs')
        // log(microABC.length + ' ABC micro outputs')
      } catch (e) {
        logError('MICROKEY FETCH ERROR', e)
        return dispatch('buildTxError', 'Connection error while retrieving pre-split outputs. Check your entries and try again or contact the system administrator if this problem persists.' + JSON.stringify(e))
      }
    }
    log('SV microkeys', svMicrocount, svMicroUtxos)
    log('ABC microkeys', abcMicrocount, abcMicroUtxos)

    const txfee = parsedUtxos.length * 5 * 250 // 5 satoshis per byte, each output is 250 bytes
    let txbuiltSV, rawtxSV, txbuiltABC, rawtxABC
    let outputsSV = []
    let outputsABC = []
    outputsSV.push({address: svFormatted.cashaddr, amount: totalToSend})
    outputsSV.push({address: svFundingAddress, amount: microAmountSatoshi})
    outputsABC.push({address: abcFormatted.cashaddr, amount: totalToSend})
    outputsABC.push({address: abcFundingAddress, amount: microAmountSatoshi})
    // log('SV Outputs Final', outputsSV)
    // log('ABC Outputs Final', outputsABC)

    try {
      txbuiltSV = buildTx(parsedUtxos.concat(svMicroUtxos), outputsSV, txfee, privKeysToSpend.concat(svMicroKeys), profitAddress, longestChainHeight, 'sv', longestChain)
      txbuiltABC = buildTx(parsedUtxos.concat(abcMicroUtxos), outputsABC, txfee, privKeysToSpend.concat(abcMicroKeys), profitAddress, longestChainHeight, 'abc', longestChain)

      rawtxSV = txbuiltSV.serialize()
      rawtxABC = txbuiltABC.serialize()
      log(txbuiltSV, txbuiltABC)
      log(rawtxSV, rawtxABC)
      commit('setRawtxSV', rawtxSV)
      commit('setRawtxABC', rawtxABC)
    } catch (e) {
      logError('TX BUILD ERROR', e)
      return dispatch('buildTxError', 'There was an unknown error while building transactions. Please contact the system administrator if this problem persists.' + JSON.stringify(e))
    }
    // console.log(logObj)
    if (!debug) {
      try {
        const resultSV = await api.sendABC({rawtx: rawtxABC, logObj: logObj, email: email})
        commit('setSentNotificationABC', 'BROADCASTED ABC TX ID:' + resultSV.txid)
      } catch (error) {
        logError('ERROR BROADCASTING ABC', error.message)
        let errorFriendly = error.message || error || 'UNKNOWN ERROR'
        if (errorFriendly.indexOf('txn-mempool-conflict') > -1) {
          errorFriendly = 'txn-mempool-conflict, which means you need to wait for your previous sweep to confirm on both blockchains before you start another sweep'
        } else if (errorFriendly.indexOf('Missing inputs') > -1) {
          errorFriendly = 'Missing inputs, which means you either need to wait for your previous sweep to confirm on both blockchains before you start another sweep, or that you may be trying to sweep keys that are already split.'
        }
        commit('setSentNotificationABC', errorFriendly)
      }

      try {
        const resultABC = await api.sendSV({rawtx: rawtxSV, logObj: logObj, email: email})
        commit('setSentNotificationSV', 'BROADCASTED SV TX ID:' + resultABC.txid)
      } catch (error) {
        logError('ERROR BROADCASTING SV', error.message)
        let errorFriendly = error.message || error || 'UNKNOWN ERROR'
        if (errorFriendly.indexOf('txn-mempool-conflict') > -1) {
          errorFriendly = 'txn-mempool-conflict, which means you need to wait for your previous sweep to confirm on both blockchains before you start another sweep'
        } else if (errorFriendly.indexOf('Missing inputs') > -1) {
          errorFriendly = 'Missing inputs, which means you either need to wait for your previous sweep to confirm on both blockchains before you start another sweep, or that you may be trying to sweep keys that are already split.'
        }
        commit('setSentNotificationSV', errorFriendly)
      }
    }
    commit('stopGlobalFetch')
  },
  takePhoto: async ({state, rootState, commit, dispatch}) => {
    window.QRScanner.scan((err, contents) => {
      if (err) {
        alert(err._message)
        logError('CAMERA ERROR', err._message)
      }
      if (contents) {
        alert('QR Scan result: ' + contents)
        if (state.cameraMode === 'paper') {
          commit('addPrivateKey', contents)
        } else if (state.cameraMode === 'sv') {
          commit('setSvAddress', contents)
        } else if (state.cameraMode === 'abc') {
          commit('setAbcAddress', contents)
        }
        dispatch('exitScanner')
      }
    })
  },
  exitScanner: async ({state, rootState, commit, dispatch}) => {
    window.QRScanner.destroy((status) => {
      document.getElementById('appDiv').style.display = 'flex'
      document.getElementById('qr-controls').style.display = 'none'
    })
  },
  openCamera: async ({state, rootState, commit, dispatch}, mode) => {
    document.getElementById('appDiv').style.display = 'none'
    document.getElementById('qr-controls').style.display = 'flex'
    commit('setCameraMode', mode)
    window.QRScanner.destroy(function (stats) {
      window.QRScanner.prepare((err, status) => {
        if (err) {
          // here we can handle errors and clean up any loose ends.
          logError('QR SCAN ERROR', err)
        }
        if (status.authorized) {
          // W00t, you have camera access and the scanner is initialized.
          // window.QRScanner.show() should feel very fast.

          window.QRScanner.show()
        } else if (status.denied) {
          // The video preview will remain black, and scanning is disabled. We can
          // try to ask the user to change their mind, but we'll have to send them
          // to their device settings with `window.QRScanner.openSettings()`.
          window.QRScanner.openSettings()
        } else {
          // we didn't get permission, but we didn't get permanently denied. (On
          // Android, a denial isn't permanent unless the user checks the "Don't
          // ask again" box.) We can ask again at the next relevant opportunity.

        }
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
