import _ from 'lodash'

const state = {
  globalFetch: false,
  fetchStatus: [],
  status: '',
  keyToFetch: {}
}

const getters = {
  getFetchApiErrors: (state) => {
    return _.filter(state.fetchStatus, status => status.code === 2)
  },
  getFetchApiLoading: (state) => {
    return _.filter(state.fetchStatus, status => status.code === 0)
  }
}

const mutations = {
  startGlobalFetch: (state) => {
    state.globalFetch = true
  },
  stopGlobalFetch: (state) => {
    state.globalFetch = false
  },
  setFetchStatus: (state, value) => {
    state.fetchStatus = value
  },
  setKeyToFetch: (state, value) => {
    state.keyToFetch = value
  },
  setStatus: (state, value) => {
    state.status = value
  }
}

const actions = {
  startFetching: ({ state, rootState, commit, dispatch }, key) => {
    const statuses = state.fetchStatus
    const pool = _.find(rootState.MyPools.pools, item => item._id === key.myPoolsId)
    const status = {
      key,
      pool,
      code: 0,
      status: 'Loading...'
    }
    alert(state.globalFetch, `Fetching ${status.key.name} key`, 'info', dispatch)
    if (!checkFetchingStatus(statuses, status)) {
      return commit('setFetchStatus', [ ...statuses, status ])
    }
    if (checkFetchingStatus(statuses, status)) {
      const data = _.map(statuses, item => item.key.id === status.key.id ? status : item)
      return commit('setFetchStatus', data)
    }
  },
  successFetching: ({ state, rootState, commit, dispatch }, key) => {
    const statuses = state.fetchStatus
    const status = _.find(statuses, item => item.key.id === key.id)
    const newStatus = {
      key: status.key,
      pool: status.pool,
      code: 1,
      status: 'Success'
    }
    alert(state.globalFetch, `Successfully fetched ${status.key.name}`, 'success', dispatch)
    const successDataStatus = _.map(statuses, item => item.key.id === status.key.id ? newStatus : item)
    commit('setFetchStatus', successDataStatus)
  },
  errorFetching: ({ state, rootState, commit, dispatch }, { key, err }) => {
    const statuses = state.fetchStatus
    const status = _.find(statuses, item => item.key.id === key.id)
    const newStatus = {
      key: status.key,
      pool: status.pool,
      code: 2,
      status: 'Error',
      error: err.message
    }
    alert(state.globalFetch, `Error fetching ${status.key.name}`, 'error', dispatch)
    const data = _.map(statuses, item => item.key.id === status.key.id ? newStatus : item)
    return commit('setFetchStatus', data)
  },
  clearFetching: ({ state, rootState, commit, dispatch }, key) => {
    const statuses = state.fetchStatus
    const status = _.find(statuses, item => item.key.id === key.id)
    const data = _.filter(statuses, item => item.key.id !== status.key.id)
    return commit('setFetchStatus', data)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

const checkFetchingStatus = (statuses, status) => {
  return _.find(statuses, item => item.key.id === status.key.id)
}
const alert = (globalFetch, message, theme, dispatch) => {
  if (!globalFetch) {
    dispatch('openAlert', {
      theme,
      message,
      timer: 2500
    })
  }
}
