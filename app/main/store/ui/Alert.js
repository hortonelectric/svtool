import EventBus from '../../lib/event-bus.js'

const state = {
  snotify: {},
  open: false,
  theme: 'info',
  message: '',
  timer: 0
}

const getters = {
}

const mutations = {
  storeSnotify: (state, value) => {
    state.snotify = value
  },
  openAlert: (state, value) => {
    state.open = true
    state.theme = value.theme || 'info'
    state.message = value.message || ''
    state.timer = value.timer || 0
  },
  closeAlert: (state) => {
    state.open = false
  }
}

const actions = {
  openAlert: (context, value) => {
    EventBus.$emit('snotify', {
      theme: value.theme || 'info',
      message: value.message || '',
      timer: value.timer || 2000
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
