// import { isNative } from '../../lib/native.js'

const state = {
  left: false,
  right: false,
  routes: [
    {
      header: 'SV Tool',
      divider: true,
      items: [
        { title: 'Home', route: 'instructions', icon: 'supervisor_accounts' }
      ]
    }
  ]
}

const getters = {}

const mutations = {
  openDrawerLeft: state => {
    state.left = true
  },
  closeDrawerLeft: state => {
    state.left = false
  },
  openDrawerRight: state => {
    state.right = true
  },
  closeDrawerRight: state => {
    state.right = false
  }
}

const actions = {}

export default {
  state,
  getters,
  mutations,
  actions
}
