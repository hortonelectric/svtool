const state = {
  privateKey: '',
  modal: false,
  error: ''
}

const getters = {
}

const mutations = {
  updateAddPrivateKeyText: (state, value) => {
    state.privateKey = value
  },
  setPKError: (state, value) => {
    state.error = value
  },
  openAddPrivateKeyModal: state => {
    state.error = ''
    state.modal = true
  },
  closeAddPrivateKeyModal: state => {
    state.privateKey = ''
    state.error = ''
    state.modal = false
  }
}

const actions = {
}

export default {
  state,
  getters,
  mutations,
  actions
}
