const state = {
  modal: false
}

const getters = {
}

const mutations = {
  openInfoSheetModal: state => {
    state.modal = true
  },
  closeInfoSheetModal: state => {
    state.modal = false
  }
}

const actions = {
}

export default {
  state,
  getters,
  mutations,
  actions
}
