import AES from 'crypto-js/aes'
import enc from 'crypto-js/enc-utf8'

import {setStoreErrorHandling, getStoreErrorHandling} from '../errors/storage.js'

const encrypt = (data, password) => AES.encrypt(data, password).toString()
const decrypt = (data, password) => AES.decrypt(data, password).toString(enc)

export const set = (key, value, password) => {
  try {
    setStoreErrorHandling(key, value, password)
    return localStorage.setItem(key, encrypt(JSON.stringify(value), password))
  } catch (error) {
    throw new Error(error)
  }
}
export const get = (key, password) => {
  try {
    getStoreErrorHandling(key, password)
    return JSON.parse(decrypt(localStorage.getItem(key), password))
  } catch (error) {
    throw new Error(error)
  }
}

export const getEncryptedKey = (key) => localStorage.getItem(key)
export const setEncryptedKey = (key, value) => localStorage.setItem(key, value)
export const deleteEncryptedKey = (key) => localStorage.removeItem(key)
export const clear = () => localStorage.clear()
