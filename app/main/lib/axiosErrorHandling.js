export default (error) => {
  if (error.response) {
    return error.response.data
  } else if (error.request) {
    return {error: 'Connection Refused'}
  } else {
    return {error: 'Unknown Error'}
  }
}
