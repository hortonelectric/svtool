export const isNative = () => {
  try {
    return (typeof require('nw.gui') !== 'undefined')
  } catch (e) {
    return false
  }
}
