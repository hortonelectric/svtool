const QRCode = require('qrcode')

export default async (secret) => {
  try {
    return await QRCode.toDataURL(secret)
  } catch (error) {
    throw error
  }
}
