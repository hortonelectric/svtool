import _ from 'lodash'
import sb from 'satoshi-bitcoin'
import moment from 'moment'
import Big from 'big.js'

export const roundOff = (value) => {
  if (!value) {
    return 0
  }
  return Math.round(value * 100) / 100
}

export const roundOffSatoshi = (value) => {
  if (!value) {
    return 0
  }
  return round(value, 8)
}

export const roundOffFiat = (value) => {
  if (!value) {
    return 0
  }
  return round(value, 2)
}

export const convertToPercentage = (value) => {
  if (!value) {
    return 0
  }
  return value * 100
}

export const formatTime = (value) => {
  if (!value) {
    return ''
  }
  return moment(value).format('MMMM Do YYYY, h:mm:ss a')
}

export const getBtcValue = (value) => {
  if (!value || !_.isNumber(value)) {
    return 0
  }
  return sb.toBitcoin(roundOff(value))
}

export const getEthereumValue = (value) => {
  if (!value || !_.isNumber(value)) {
    return 0
  }
  const wei = new Big(value)
  return wei.times(0.000000000000000001).toPrecision()
}

export const convertBtcFromSatoshi = (value) => {
  if (!value || !_.isNumber(value)) {
    return 0
  }
  return sb.toBitcoin(value)
}

export const convertEthereumFromWei = (value) => {
  if (!value || !_.isNumber(value)) {
    return 0
  }
  const wei = new Big(value)
  return wei.times(0.000000000000000001).toPrecision()
}

export const checkCurrencyAndConvert = (data) => {
  switch (data.currency) {
    case 'Bitcoin':
      return convertBtcFromSatoshi(data.balance || data.amount)
    case 'Ethereum':
      return convertEthereumFromWei(data.balance || data.amount)
    case 'bitcoin':
      return convertBtcFromSatoshi(data.balance || data.amount)
    case 'ethereum':
      return convertEthereumFromWei(data.balance || data.amount)
    case 'Hulmu':
      return convertEthereumFromWei(data.balance || data.amount)
    default:
      return convertEthereumFromWei(data.balance || data.amount)
  }
}

const round = (number, precision) => {
  var shift = function (number, precision, reverseShift) {
    if (reverseShift) {
      precision = -precision
    }
    var numArray = ('' + number).split('e')
    return +(numArray[0] + 'e' + (numArray[1] ? (+numArray[1] + precision) : precision))
  }
  return shift(Math.round(shift(number, precision, false)), precision, true)
}
