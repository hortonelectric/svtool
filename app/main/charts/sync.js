import _ from 'lodash'
import Highcharts from 'highcharts'
import moment from 'moment'

export default (container, data) => {
  const formatData = data => {
    return _.map(data, item => {
      return {
        x: item.timestamp,
        y: item.btc,
        usd: item.usd
      }
    })
  }

  return Highcharts.chart(container, {
    chart: {
      type: 'line'
    },
    title: {
      text: 'Sync History Chart'
    },
    xAxis: {
      title: {
        text: 'Time'
      },
      type: 'datetime'
    },
    yAxis: {
      title: {
        text: 'Value (BTC)'
      }
    },
    tooltip: {
      formatter: function () {
        const date = moment(this.x).format('MMMM Do YYYY, h:mm:ss a')
        return `${date}
          <br /> BTC: <b>${this.y}</br>
          <br /> USD: <b>${this.point.usd}</br>`
      }
    },
    plotOptions: {
      line: {
        dataLabels: {
          enabled: true
        }
      }
    },
    series: [{
      name: 'BTC',
      data: formatData(data)
    }]
  })
}
