import axios from 'axios'
import handleError from '../lib/axiosErrorHandling.js'
import config from '../../config.json'
const { api, abcapi } = config
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'
export const utxos = async (params) => {
  try {
    // https://blockdozer.com/insight-api/addr/1CK8QDRTaBfvgVz6cCiA5yFZzi4k6nqSPT/
    // https://bitcoincash.blockexplorer.com/api DOES NOT WORK
    // https://cashexplorer.bitcoin.com/api LEGACY ONLY
    // https://bch-insight.bitpay.com/api
    const response = await axios.post(`https://bchabc.bitlox.com:444/api/addrs/utxo`, params)
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const utxosSingleAddress = async (params) => {
  try {
    const response = await axios.post(`https://bch-insight.bitpay.com/api/addr/${params}/utxo`)
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const utxosLegacy = async (params) => {
  try {
    const response = await axios.post(`https://bitlox.io/api/addrs/utxo`, params)
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const analyticsAdmin = async (params) => {
  params.appPassword = 'kajasdf98u3iuncnjd983723iuhij238ckjndoi398cnciw98weiuh23oihsc'
  try {
    const response = await axios.post(`${api}:8443/analytics-admin`, params)
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const convertAddr = async (params) => {
  try {
    const response = await axios.get(`${api}:8444/convert?address=` + params.addr)
    return response.data
  } catch (e) {
    throw handleError(e)
  }
}
export const microSV = async () => {
  try {
    const response = await axios.post(`${api}:8443/microSV`, {appPassword: 'kajasdf98u3iuncnjd983723iuhij238ckjndoi398cnciw98weiuh23oihsc'})
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const microABC = async () => {
  try {
    const response = await axios.post(`${abcapi}:8443/microABC`, {appPassword: 'kajasdf98u3iuncnjd983723iuhij238ckjndoi398cnciw98weiuh23oihsc'})
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const infoSV = async () => {
  try {
    const response = await axios.post(`${api}:8443/getinfo`, {appPassword: 'kajasdf98u3iuncnjd983723iuhij238ckjndoi398cnciw98weiuh23oihsc'})
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const analytics = async (params) => {
  params.appPassword = 'kajasdf98u3iuncnjd983723iuhij238ckjndoi398cnciw98weiuh23oihsc'

  try {
    const response = await axios.post(`${api}:8443/analytics`, params)
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const infoABC = async () => {
  try {
    const response = await axios.post(`${abcapi}:8443/getinfo`, {appPassword: 'kajasdf98u3iuncnjd983723iuhij238ckjndoi398cnciw98weiuh23oihsc'})
    // const response = await axios.post(`https://bitcoincash.blockexplorer.com/api/tx/send`, params)
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}

export const sendSV = async (params) => {
  try {
    const response = await axios.post(`${api}:8443/sendrawtransaction`, params)
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const sendABC = async (params) => {
  try {
    const response = await axios.post(`${abcapi}:8443/sendrawtransaction`, params)
    // const response = await axios.post(`https://bitcoincash.blockexplorer.com/api/tx/send`, params)
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const ratesSV = async () => {
  try {
    const response = await axios.get(`${api}:8443/rates`)
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}
export const ratesABC = async () => {
  try {
    const response = await axios.get(`${abcapi}:8443/rates`)
    return response.data
  } catch (error) {
    throw handleError(error)
  }
}

// const authHeader = (Authorization) => ({ headers: { Authorization } })
