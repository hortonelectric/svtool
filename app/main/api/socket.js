import _ from 'lodash'
import config from '../../config.json'
import io from 'socket.io-client'
import { checkCurrencyAndConvert } from '../lib/helpers.js'
const { ws } = config

export const connect = ({ rootState, commit, dispatch }, auth) => {
  const socket = io(`${ws}?authHeader=${auth}`)
  const id = rootState.User.id

  socket.on('connect', function () {
    console.log('conencted')
    socket.emit('start') // tell the server we are here
  })
  socket.on('event', function (data) {
    console.log(data)
  })
  // show an overlay, as the game cannot go on without a socket
  socket.on('disconnect', function () {
  // $rootScope.socketConnected = false
  })
  socket.on('multiple-sockets', function () {
  // $rootScope.multipleSockets = true
  })
  socket.on('socket-error', function (data) {
  // $rootScope.socketConnected = false
  })
  socket.on('socket-initialized', function (data) {
  // $rootScope.socketConnected = true
  })

  // for individual players
  socket.on('user-update', function (data) {
  // $rootScope.USER = data
  // $localStorage.USER = data
  // $rootScope.$broadcast('user-update', data)
  })

  socket.on('exchange-rates', function (data) {
  // $rootScope.EXCHANGERATES = data
  // $rootScope.$broadcast('exchange-rates', data)
  })

  socket.on('user-login', function (data) {
  // $rootScope.$broadcast('user-login')
  })

  socket.on('deposit', function (data) {
    // $rootScope.socketConnected = true
  })
  socket.on('withdraw-error', function (data) {
  // $rootScope.$broadcast('withdraw-error', data)
  })
  socket.on('withdraw-success', function (data) {
  // $rootScope.$broadcast('withdraw-success', data)
  })

  socket.on('update-public-pools', function (data) {
    return commit('setPools', data)
  })

  // Wallet updates sockets
  socket.on('wallet-update', function (result) {
    const data = _.map(result, item => Object.assign({}, item, { balance: item.balance || 0 }))
    return commit('setWallets', data)
  })

  socket.on(`wallet/${id}/deposit/unconfirmed`, result => {
    const currency = _.startCase(result.currency.toString())
    dispatch('openAlert', {
      theme: 'info',
      message: `${currency} transaction has been received but not yet been confirmed with a balance of ${checkCurrencyAndConvert(result)}`,
      timer: 15000
    })
  })

  socket.on(`wallet/${id}/deposit/confirmed`, result => {
    const currency = _.startCase(result.currency.toString())
    const transactions = rootState.Wallets.transactions
    dispatch('openAlert', {
      theme: 'success',
      message: `${currency} transaction has been received and confirmed, balance updated`,
      timer: 15000
    })
    commit('setTransactions', [ result, ...transactions ])
  })

  socket.on(`wallet/${id}/withdraw/unconfirmed`, result => {
    const currency = _.startCase(result.currency.toString())
    dispatch('openAlert', {
      theme: 'info',
      message: `Withdrawal of ${currency} with an amount of ${checkCurrencyAndConvert(result)} is processed, wallet balance updated.`,
      timer: 15000
    })
  })

  socket.on(`wallet/${id}/withdraw/confirmed`, result => {
    const currency = _.startCase(result.currency.toString())
    const transactions = rootState.Wallets.transactions
    dispatch('openAlert', {
      theme: 'success',
      message: `Withdrawal of ${currency} successfully processed.`,
      timer: 15000
    })
    commit('setTransactions', [ result, ...transactions ])
  })

  return socket
}
