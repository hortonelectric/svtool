import alert from './alert.js'

export default (err, dispatch) => {
  const { error, message } = err
  if (message && message !== 'Validation Error') {
    return alert(message, dispatch)
  }
  if (error === 'Connection Refused') {
    return alert('Cannot connect to server, please try again later.', dispatch)
  }
  if (error === 'Unknown Error') {
    return alert('Something went wrong, please try again later.', dispatch)
  }
  console.log(err)
}
