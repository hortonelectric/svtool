const errorHandling = (key, password) => {
  if (!key) {
    throw new Error('No storage provided')
  }
  if (typeof key !== 'string') {
    throw new Error('Expected string on storage variable')
  }
  if (!password) {
    throw new Error('No Encryption Key Provided')
  }
  if (typeof password !== 'string') {
    throw new Error('Expected string on password variable')
  }
}

export const setStoreErrorHandling = (key, value, password) => {
  errorHandling(key, password)
  if (!value) {
    throw new Error('No value to be encrypted')
  }
}

export const getStoreErrorHandling = errorHandling
