import handler from './index.js'
// import alert from './alert.js'
import { serverSideValidation } from '../validations/signup.js'

export default (err, store) => {
  const { message, statusCode, validation } = err
  console.error(message)
  if (statusCode === 400 && validation.source) {
    return serverSideValidation(store, validation)
  }
  if (statusCode === 409) {
    return message// alert(message, store.dispatch)
  }
  handler(err, store.dispatch)
}
