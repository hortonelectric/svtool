import handler from './index.js'

export default (err, store) => {
  const { error, statusCode } = err
  if (error === 'Unauthorized' && statusCode === 401) {
    return store.commit('setLoginPasswordError', 'Invalid Username and/or Password')
  }
  handler(err, store.dispatch)
}
