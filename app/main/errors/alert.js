export default (message, dispatch) => {
  dispatch('openAlert', {
    theme: 'error',
    message,
    timer: 2000
  })
}
