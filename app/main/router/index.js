import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
// import _ from 'lodash'
// import { isNative } from '../lib/native.js'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      component: require('@/components/Main'),
      children: [
        {
          path: '/',
          redirect: 'claim'
        },
        {
          path: 'claim',
          name: 'claim',
          component: require('@/components/Claim'),
          beforeEnter: (to, from, next) => {
            store.dispatch('refreshRates')
            return next()
          }
        },
        {
          path: 'analytics',
          name: 'analytics',
          component: require('@/components/office/Analytics'),
          beforeEnter: (to, from, next) => {
            store.dispatch('refreshAnalytics')
            return next()
          }
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  // const { exist, auth, twofa } = store.state.User

  return next()
})

// const unguardedRoutes = to =>
//   to.path === '/logout'

export default router
