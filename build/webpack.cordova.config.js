process.env.BABEL_ENV = 'main'

const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin')
const BabelMinifyWebpackPlugin = require('babel-minify-webpack-plugin')
var WebpackGitHash = require('webpack-git-hash');

let commitHash = require('child_process')
  .execSync('git rev-parse --short HEAD')
  .toString();
var fs = require('fs')
const utils = require('./utils')
const { dependencies } = require('../app/package')
const vueLoaderConfig = require('./vue-loader.config')
const isProduction = process.env.NODE_ENV === 'production'
const buildFolder = process.env.BUILD_FOLDER

let config = {
  devtool: isProduction ? false : '#cheap-module-eval-source-map',  
  entry: {
    main: path.join(__dirname, '../app/main/main')
  },
  output: {
    path: path.join(__dirname, '../'+buildFolder),
    filename: '[name].[githash].js'
  }, 
  // node: {
  //   Buffer: true,
  //   crypto: true
  // },
  module: {    
    rules: [
      ...utils.styleLoaders({
        sourceMap: !isProduction,
        extract: isProduction
      }),
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueLoaderConfig
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          query: {
            limit: 10000,
            name: 'img/[name].[hash:7].[ext]'
          }
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'media/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          query: {
            limit: 10000,
            name: 'fonts/[name].[hash:7].[ext]'
          }
        }
      },
      {
        test: /\.(txt|csv)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'data/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(\.json$)(\?.*)?$/,
        loader: 'json-loader',
        options: {
          limit: 10000,
          name: 'data/[name].[hash:7].[ext]'
        }
      },      
    ]
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      '@': path.join(__dirname, '../app/main'),
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  plugins: [
    new ExtractTextWebpackPlugin('style.css'),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.join(__dirname, '../app/main/index.ejs'),
      minify: isProduction ? {
        collapseWhitespace: true,
        removeAttributeQuotes: true,
        removeComments: true
      } : undefined
    }),
    new WebpackGitHash(),
    new webpack.DefinePlugin({
      __COMMIT_HASH__: JSON.stringify(commitHash),
    })
  ]
}

if (isProduction) {
  config.devtool = false
  config.plugins.push(
    new BabelMinifyWebpackPlugin(),
    // http://vuejs.github.io/vue-loader/en/workflow/production.html
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"'
    })
  )
} else {
  config.plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  )
}

module.exports = config
