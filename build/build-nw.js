
const fs = require('fs-extra')
const ora = require('ora')
const path = require('path')
const chalk = require('chalk')
const { spawn } = require('child_process')
const npmWhich = require('npm-which')(__dirname)
const webpack = require('webpack')
const argv = require('yargs').argv
if(argv.production) { 
  process.env.NODE_ENV = 'production'
  console.log(chalk.red("PRODUCTION BUILD, MINIFIED"))
} else {
  console.log(chalk.green("DEV BUILD, NOT MINIFIED"))
}
process.env.BUILD_FOLDER = 'dist'

const webpackMainConfig = require('./webpack.cordova.config')

const buildPath = npmWhich.sync('build')

function cleanDist () {
  return fs.emptydir(path.resolve(__dirname, '../dist'))
}

function cleanBuild () {
  return fs.emptydir(path.resolve(__dirname, '../releases'))
}

function distManifest () {
  return new Promise((resolve, reject) => {
    fs.readJson(path.resolve(__dirname, '../app/package.json'), (err, data) => {
      if (err) reject(err)
      else {
        data['main'] = 'index.html'
        fs.outputJson(path.resolve(__dirname, '../dist/package.json'), data, (err) => {
          if (err) reject(err)
          else resolve()
        })
      }
    })
  })
}

function distNodeModules () {
  return new Promise((resolve, reject) => {
    const src = path.resolve(__dirname, '../app/node_modules')
    if (fs.existsSync(src)) {
      fs.copy(src, path.resolve(__dirname, '../dist/node_modules'), (err) => {
        if (err) reject(err)
        else resolve()
      })
    } else {
      resolve()
    }
  })
}

function packMain () {
  return pack(webpackMainConfig)
}

function pack (config) {
  return new Promise((resolve, reject) => {
    webpack(config, (err, stats) => {
      if (err) reject(err)
      else if (stats.hasErrors()) reject(stats.toString({ chunks: false, colors: true }))
      else resolve()
    })
  })
}

function build () {
  let os = ''
  let arch = ''

  switch (process.platform) {
    case 'linux':
      os = '--linux'
      break
    case 'darwin':
      os = '--mac'
      break
    case 'win32':
    default:
      os = '--win'
  }

  switch (process.arch) {
    case 'ia32':
      arch = '--x86'
      break
    case 'x64':
    default:
      arch = '--x64'
  }

  spawn(buildPath, [os, arch, 'dist'], { stdio: 'inherit' })
}
const spinner = ora('Building...')
spinner.start()

Promise
  .all([cleanDist(), cleanBuild()])
  .then(() => {
    Promise
      .all([distManifest(), distNodeModules(), packMain()])
      .then(() => {
        build()
        spinner.stop()
      })
      .catch((err) => {
        console.error(err)
        spinner.stop()
      })
  })
  .catch((err) => {
    console.error(err)
    spinner.stop()
  })
