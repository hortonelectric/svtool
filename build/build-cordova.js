'use strict'

const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const argv = require('yargs').argv
if(argv.production) { 
  process.env.NODE_ENV = 'production'
  console.log(chalk.red("PRODUCTION BUILD, MINIFIED"))
} else {
  console.log(chalk.green("DEV BUILD, NOT MINIFIED"))
}
process.env.BUILD_FOLDER = 'www'
const webpackConfig = require('./webpack.cordova.config')

const spinner = ora('Building...')
spinner.start()

rm(path.join(path.resolve(__dirname, '../www'), 'static'), err => {
  if (err) throw err
  webpack(webpackConfig, (err, stats) => {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false, // If you are using ts-loader, setting this to true will make TypeScript errors show up during build.
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      process.exit(1)
    }

    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ))
  })
})
