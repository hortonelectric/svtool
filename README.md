# Prescun

> Exchange parser application

# install dependencies
npm install

# Build the app on development. Serve with hot reload at localhost:9080
npm run dev

# Build NW.js application for production ('--x64' or '--x86') ('--linux', '--mac', '--win')
npm run build

ex:  npm run build --x64 --win 
